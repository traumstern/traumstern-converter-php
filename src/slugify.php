<?php

use Cocur\Slugify\Slugify;

function movieTitleToSlug($movieTitle): string
{
    $slugify = new Slugify();
    return $slugify->slugify(strtolower($movieTitle));
}
