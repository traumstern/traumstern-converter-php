<?php
require __DIR__ . "/../vendor/autoload.php";

// Load environment variables from .env file
use Traumstern\ConverterPhp\Router;
use function Traumstern\ConverterPhp\initPDO;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();

// Initialize the database connection pool
$pdo = initPDO();

// Create router instance
$router = new Router();

// Define routes
$router->get('/movies', function () use ($pdo) {
    try {
        // Get a connection from the pool
        $movies = fetchMovies($pdo);
        $augmentedMovies = array_map('augmentWithId', $movies);
        header('Content-Type: application/json');
        echo json_encode($augmentedMovies);
        exit;
    } catch (Exception $e) {
        // Handle errors
        error_log($e);
        http_response_code(500);
        echo json_encode(['error' => 'Internal Server Error']);
        exit;
    }
});

$router->get('/dates', function () use ($pdo) {
    try {
        $spielplaene = fetchDatesWithMovies($pdo);
        // Augment movies with IDs
        $augmentedSpielplaene = array_map(function ($spielplan) {
            return [
                'date' => $spielplan['date'],
                'movies' => array_map(function ($movie) {
                    return [
                        'startTime' => $movie['startTime'],
                        'id' => movieTitleToSlug($movie['titel'])
                    ];
                }, $spielplan['movies'])
            ];
        }, $spielplaene);
        header('Content-Type: application/json');
        echo json_encode($augmentedSpielplaene);
        exit;
    } catch (Exception $e) {
        // Handle errors
        error_log($e);
        http_response_code(500);
        echo json_encode(['error' => 'Internal Server Error']);
        exit;
    }
});

// Resolve the request
$router->resolve();