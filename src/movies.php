<?php

function fetchMovies($connection): array
{
    $query = "
SELECT DISTINCT JSON_OBJECT(
'titel', CONVERT(titel USING utf8), 
'zusatz', CONVERT(zusatz using utf8),
'header', CONVERT(header using utf8),
'beschreibung', CONVERT(beschreibung using utf8)
) AS json_result
FROM _traumstern_film
JOIN _traumstern_zeitpaar ON fk_film_id = _traumstern_film.id
WHERE _traumstern_zeitpaar.start > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
GROUP BY titel;
";

    $stmt = $connection->prepare($query);
    $stmt->execute();

    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return array_map(function ($result) {
        return json_decode($result['json_result'], true);
    }, $results);
}

function augmentWithId($movie): array
{
    return $movie + ['id' => movieTitleToSlug($movie['titel'])];
}