<?php
function fetchDatesWithMovies($connection)
{
    // The Common Table Expression up front is used to iterate between start datetime and end date,
    // so that we actually fetch all dates, not just the start date.
    // The SELECT at the end can operate on this modified data set to produce the result including
    // all dates and movie infos
    $query = "
        WITH RECURSIVE StartDates AS (
        SELECT start as date, fk_film_id FROM _traumstern_zeitpaar WHERE start > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
        UNION ALL
        SELECT DATE_ADD(start, INTERVAL 1 DAY) as date, fk_film_id FROM (
            SELECT start, ende, fk_film_id
            FROM _traumstern_zeitpaar
            WHERE start > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
            OR ende > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
        ) as DateRange
        WHERE DATE_ADD(start, INTERVAL 1 DAY) < ende
    )
    SELECT DATE_FORMAT(date, '%Y-%m-%d') as date, JSON_ARRAYAGG(
            JSON_OBJECT(
                    'titel', CONVERT(titel using utf8),
                    'startTime', DATE_FORMAT(date, '%H:%i')
            )
           ) as movies
    FROM StartDates
             JOIN _traumstern_film ON _traumstern_film.id = fk_film_id
    GROUP BY DATE_FORMAT(date, '%Y-%m-%d');
    ";

    $stmt = $connection->prepare($query);
    $stmt->execute();

    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($results as &$result) {
        $result['movies'] = json_decode($result['movies'], true);
    }

    return $results;
}
