<?php

namespace Traumstern\ConverterPhp;

class Router
{
    private array $routes = [];

    public function get($path, $callback): void
    {
        $this->routes[$path] = $callback;
    }

    public function resolve(): void
    {
        $path = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        $callback = $this->routes[$path] ?? false;

        if ($callback && $method === 'GET') {
            $callback();
        } else {
            http_response_code(404);
            echo json_encode(['error' => 'Not Found']);
            exit;
        }
    }
}